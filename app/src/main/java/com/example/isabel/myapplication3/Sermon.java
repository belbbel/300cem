package com.example.isabel.myapplication3;

//CREATED BY ISABEL G1578672N


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

public class Sermon extends AppCompatActivity {

    Toolbar mToolbar;
    ListView mListView;

    String[] pastorNames = {"Bill Hybels",
            "Joel Osteen",
            "Joseph Prince",
            "Kerry Shook",
            "Larry Mack",
            "Priscilla Shirer",
            "Rick Warren",
            "Wayne Cordeirio"};

    int[] pastorPhotos = {R.drawable.bill,
            R.drawable.joel,
            R.drawable.joseph,
            R.drawable.kerry,
            R.drawable.larry,
            R.drawable.priscilla,
            R.drawable.rick,
            R.drawable.wayne};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sermon);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setTitle(getResources().getString(R.string.app_name));
        mListView = (ListView) findViewById(R.id.listview);
        MyAdapter myAdapter = new MyAdapter(Sermon.this, pastorNames, pastorPhotos);
        mListView.setAdapter(myAdapter);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent mIntent = new Intent(Sermon.this, Videolist.class);

                startActivity(mIntent);
            }
        });
    }
}
