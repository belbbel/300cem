package com.example.isabel.myapplication3;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class BibleAdapter extends ArrayAdapter<String> {

    String[] chapters;
    int[] images;
    Context bContext;

    public BibleAdapter(Context context, String[] chapterNames, int[] logoImage) {
        super(context, R.layout.listview_item_bible);
        this.chapters = chapterNames;
        this.images = logoImage;
        this.bContext = context;
    }


    @Override
    public int getCount() {
        return chapters.length;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder mViewHolder = new ViewHolder();
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) bContext.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.listview_item_bible, parent, false);
            mViewHolder.bLogo = (ImageView) convertView.findViewById(R.id.imageView2);
            mViewHolder.bChapter = (TextView) convertView.findViewById(R.id.textView2);
            convertView.setTag(mViewHolder);
        } else {
            mViewHolder = (ViewHolder) convertView.getTag();
        }
        mViewHolder.bLogo.setImageResource(images[position]);
        mViewHolder.bChapter.setText(chapters[position]);

        return convertView;
    }

    static class ViewHolder {
        ImageView bLogo;
        TextView bChapter;
    }
}

