package com.example.isabel.myapplication3;

//CREATED BY ISABEL G1578672N


import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

public class Bible extends AppCompatActivity {

    ListView blistview;

    String[] chapterNames ={
            "    Genesis",
            "    Exodus",
            "    Leviticus",
            "    Numbers",
            "    Deuteronomy",
            "    Joshua",
            "    Judges",
            "    Ruth",
            "    1 Samuel",
            "    2 Samuel",
            "    1 Kings",
            "    2 Kings",
            "    1 Chronicles",
            "    2 Chronicles",
            "    Ezra",
            "    Nehemiah",
            "    Esther",
            "    Job",
            "    Psalms",
            "    Proverbs",
            "    Ecclesiastes",
            "    Song of Solomon",
            "    Isaiah",
            "    Jeremiah",
            "    Lamentations",
            "    Ezekiel",
            "    Daniel",
            "    Hosea",
            "    Joel",
            "    Amos",
            "    Obadian",
            "    Jonah",
            "    Micah",
            "    Nahum",
            "    Habakkuk",
            "    Zephaniah",
            "    Haggai",
            "    Zechariah",
            "    Malachi"};

    int[] logoImages ={
            R.drawable.cross,
            R.drawable.cross,
            R.drawable.cross,
            R.drawable.cross,
            R.drawable.cross,
            R.drawable.cross,
            R.drawable.cross,
            R.drawable.cross,
            R.drawable.cross,
            R.drawable.cross,
            R.drawable.cross,
            R.drawable.cross,
            R.drawable.cross,
            R.drawable.cross,
            R.drawable.cross,
            R.drawable.cross,
            R.drawable.cross,
            R.drawable.cross,
            R.drawable.cross,
            R.drawable.cross,
            R.drawable.cross,
            R.drawable.cross,
            R.drawable.cross,
            R.drawable.cross,
            R.drawable.cross,
            R.drawable.cross,
            R.drawable.cross,
            R.drawable.cross,
            R.drawable.cross,
            R.drawable.cross,
            R.drawable.cross,
            R.drawable.cross,
            R.drawable.cross,
            R.drawable.cross,
            R.drawable.cross,
            R.drawable.cross,
            R.drawable.cross,
            R.drawable.cross,
            R.drawable.cross,
            R.drawable.cross
    };

    public Button newTestamentbutton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bible);

        blistview = (ListView) findViewById(R.id.chapter);
        BibleAdapter bibleAdapter = new BibleAdapter(Bible.this, chapterNames, logoImages);
        blistview.setAdapter(bibleAdapter);
        blistview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent bIntent = new Intent(Bible.this, Verse.class);



                startActivity(bIntent);
    }
});
    }


}
