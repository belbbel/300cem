package com.example.isabel.myapplication3;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

public class NewTestament extends AppCompatActivity {

    ListView blistview2;

    String[] chapterNames2 ={
                    "    Matthew",
                    "    Mark",
                    "    Luke",
                    "    John",
                    "    Acts (of the Apostles)",
                    "    Romans",
                    "    1 Corinthians",
                    "    2 Corinthians",
                    "    Galatians",
                    "    Ephesians",
                    "    Philippians",
                    "    Colossians",
                    "    1 Thessalonians",
                    "    2 Thessalonians",
                    "    1 Timothy",
                    "    2 Timothy",
                    "    Titus",
                    "    Philemon",
                    "    Hebrews",
                    "    James",
                    "    1 Peter",
                    "    2 Peter",
                    "    1 John",
                    "    2 John",
                    "    3 John",
                    "    Jude",
                    "    Revelation"};

    int[] logoImages2 ={
            R.drawable.cross,
            R.drawable.cross,
            R.drawable.cross,
            R.drawable.cross,
            R.drawable.cross,
            R.drawable.cross,
            R.drawable.cross,
            R.drawable.cross,
            R.drawable.cross,
            R.drawable.cross,
            R.drawable.cross,
            R.drawable.cross,
            R.drawable.cross,
            R.drawable.cross,
            R.drawable.cross,
            R.drawable.cross,
            R.drawable.cross,
            R.drawable.cross,
            R.drawable.cross,
            R.drawable.cross,
            R.drawable.cross,
            R.drawable.cross,
            R.drawable.cross,
            R.drawable.cross,
            R.drawable.cross,
            R.drawable.cross,
            R.drawable.cross,
            R.drawable.cross,
            R.drawable.cross,
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_testament);

        blistview2 = (ListView) findViewById(R.id.chapter2);
        BibleAdapter bibleAdapter2 = new BibleAdapter(NewTestament.this, chapterNames2, logoImages2);
        blistview2.setAdapter(bibleAdapter2);
        blistview2.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent bIntent2 = new Intent(NewTestament.this, Verse.class);

                startActivity(bIntent2);
            }
        });
    }


}
