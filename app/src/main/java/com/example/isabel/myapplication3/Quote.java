package com.example.isabel.myapplication3;

import android.app.WallpaperManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.IOException;
import java.util.Random;

import static android.widget.Toast.LENGTH_SHORT;

public class Quote extends AppCompatActivity {

    ImageView quoteview;

    Button button1;
    Button button2;

    Random r;

    Integer[] quotes = {
            R.drawable.quote1,
            R.drawable.quote2,
            R.drawable.quote3,
            R.drawable.quote4,
            R.drawable.quote5,
            R.drawable.quote6,
            R.drawable.quote7,
            R.drawable.quote8,
            R.drawable.quote9
    };

    int pickedquote = 0;
    int lastpicked = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quote);


        quoteview = (ImageView) findViewById(R.id.quoteview);

        button1 = (Button) findViewById(R.id.button1);

        button2 = (Button) findViewById(R.id.button2);

        r = new Random();

        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // avoid duplicated quote
                do {
                    pickedquote = r.nextInt(quotes.length);
                } while (pickedquote == lastpicked);

                lastpicked = pickedquote;
                // generate random quote
                quoteview.setImageResource(quotes[pickedquote]);

            }

        });

        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                WallpaperManager wallpp = WallpaperManager.getInstance(getApplicationContext());
                try{ wallpp.setResource(+ R.drawable.quote10);

                    Toast.makeText(Quote.this, "QUOTE SET AS WALLPAPER", Toast.LENGTH_LONG).show();
            }
            catch (IOException e){
                    e.printStackTrace();
            }
        };

    });



};}