package com.example.isabel.myapplication3;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.content.Intent;
import android.view.View;
import android.widget.Button;



public class Buttonswitch extends AppCompatActivity{
    public Button newTestamentbutton;

    public void init4(){
        newTestamentbutton= (Button) findViewById(R.id.newTestamentbutton);
        newTestamentbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent newt = new Intent(getApplicationContext(
),NewTestament.class);

                startActivity(newt);
            }
        });
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bible);

        init4();

    }
}