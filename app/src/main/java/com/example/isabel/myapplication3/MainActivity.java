package com.example.isabel.myapplication3;

//CREATED BY ISABEL G1578672N

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    public Button quotebutton;

    public void init(){
        quotebutton= (Button) findViewById(R.id.quotebutton);
        quotebutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent quote = new Intent(getApplicationContext(),Quote.class);

                startActivity(quote);
            }
        });
    }

    public Button biblebutton;

    public void init2(){
        biblebutton= (Button) findViewById(R.id.biblebutton);
        biblebutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent bible = new Intent(getApplicationContext(),Bible.class);

                startActivity(bible);
            }
        });
    }

    public Button sermonbutton;

    public void init3(){
        sermonbutton= (Button) findViewById(R.id.sermonbutton);
        sermonbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent sermon = new Intent(getApplicationContext(),Sermon.class);

                startActivity(sermon);
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        init();
        init2();
        init3();
    }
}
